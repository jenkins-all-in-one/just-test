stages {
      stage('Jenkinsfile-Test') {
         steps {
            script{
                //git branch: 'Your Branch name', credentialsId: 'Your crendiatails', url: ' Your BitBucket Repo URL '
                git branch: 'master', credentialsId: 'bitbucket-cred', url: 'https://bitbucket.org/jenkins-all-in-one/just-test/'
                //To read file from workspace which will contain the Jenkins Job Name ###
                def filePath = readFile "${WORKSPACE}/README.md"                   
                //To read file line by line ###
                def lines = filePath.readLines() 
      